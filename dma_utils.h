#ifndef _DMAUTILS_H
#define _DMAUTILS_H

#define RW_MAX_SIZE 0x7ffff000

#include <libxildrv.h>
#include <linux/ioctl.h>
#include <stdint.h>
#include <sys/types.h>

#define DEVICE_NAME_DEFAULT "/dev/xdma0"
#define DEVICE_C2H_NAME_DEFAULT "/dev/xdma0_c2h_0"
#define DEVICE_H2C_NAME_DEFAULT "/dev/xdma0_h2c_0"
#define DQ_LINES 16
#define DMA_ADDRESS_SMEM1 0x0
#define DMA_ADDRESS_SMEM2 0x20000000
#define SIZE_DEFAULT (64)
#define COUNT_DEFAULT (1)

ssize_t read_to_buffer (const char *fname, int fd, char *buffer, uint64_t size,
                        uint64_t base);
ssize_t write_from_buffer (const char *fname, int fd, char *buffer,
                           uint64_t size, uint64_t base);

int disable_vtc (xildev *dev, unsigned int mem);
void restore_vtc (xildev *dev, unsigned int mem, int vtc_set);
void set_delay (xildev *dev, unsigned int mem, int value, int line);
uint32_t read_delay (xildev *dev, unsigned int mem, int line);

int read_dma (const char *devname, uint64_t addr, uint64_t size,
              uint64_t offset, unsigned int *dest_buf);
int write_dma (const char *devname, uint64_t addr, uint64_t size,
               uint64_t offset, unsigned int *src_buf);

uint64_t getopt_integer (char *optarg);

#endif /* _DMAUTILS_H */
