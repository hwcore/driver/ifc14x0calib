# IFC14x0calib

Calibrates the IFC14xx DDR3 memories though the xdma device. This is compatible
with new "Non-TOSCA" firmware that implements two DDR3 memories. 

## How to build

This software is built using CMake (version 3+):

```sh
cd ifc14x0calib
mkdir build && cd build
cmake3 ../
make
```

## How to use

```sh
usage: ./ifc14x0calib [OPTIONS]

IFC14x0 - DDR3 write leveling (calibration)

  -d (--device) device (defaults to /dev/xdma0)
  -c (--devicec2h) devicec2h (defaults to /dev/xdma0_c2h_0)
  -o (--deviceh2c) deviceh2c (defaults to /dev/xdma0_h2c_0)
  -h (--help) print usage help and exit
  -v (--verbose) verbose output (1 = reg. dump, 2 = mem dump)
  -m (--memory) memory bank (1 = SMEM1, 2 = SMEM2, 3 = both and default)

Return code:
  0: DDR calibrated
  < 0: error
```
