/*
 * This file is part of the Xilinx DMA IP Core driver tools for Linux
 *
 * Copyright (c) 2016-present,  Xilinx, Inc.
 * All rights reserved.
 *
 * This source code is licensed under BSD-style license (found in the
 * LICENSE file in the root directory of this source tree)
 */

#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>

#include "dma_utils.h"
#include "ifc14x0calib_regs.h"

#define DELAY_MASK 0x01ff

static int eop_flush = 0;

/*
 * Register address for each memory
 */
unsigned int SMEM_DDR_IDELAY_MGT[2]
    = { AXI_SMEM_DDR3_IDELAY_MGT_1, AXI_SMEM_DDR3_IDELAY_MGT_2 };
unsigned int SMEM_DDR_IDELAY_VAL[2]
    = { AXI_SMEM_DDR3_IDELAY_VAL_1, AXI_SMEM_DDR3_IDELAY_VAL_2 };

uint64_t
getopt_integer (char *optarg)
{
  int rc;
  uint64_t value;

  rc = sscanf (optarg, "0x%lx", &value);
  if (rc <= 0)
    rc = sscanf (optarg, "%lu", &value);

  return value;
}

/*
 * Disable VTC (Voltage and Temperature Control) and returns the last register
 * value
 */
int
disable_vtc (xildev *dev, unsigned int mem)
{
  int vtc_read = 0;
  xil_read_reg (dev, SMEM_DDR_IDELAY_MGT[mem], (uint32_t *)&vtc_read, 0);
  int vtc_set = (vtc_read | (1 << 28));
  xil_write_reg (dev, SMEM_DDR_IDELAY_MGT[mem], (uint32_t)vtc_set, 0);
  return vtc_read;
}

/*
 * Set VTC (Voltage and Temperature Control) register value
 */
void
restore_vtc (xildev *dev, unsigned int mem, int vtc_set)
{
  xil_write_reg (dev, SMEM_DDR_IDELAY_MGT[mem], (uint32_t)vtc_set, 0);
}

/*
 * Set delay value for line
 */
void
set_delay (xildev *dev, unsigned int mem, int value, int line)
{
  int vtc_read = disable_vtc (dev, mem);
  // Assign a default value to each(j) IDELAY3 cell
  uint32_t data = ((value << 16) | (line << 12));
  // IDELAY3 value & DDR3 DQ Selection
  xil_write_reg (dev, SMEM_DDR_IDELAY_VAL[mem], (uint32_t)data, 0);
  data = (1 << 31) | (0x1 << (line));
  xil_write_reg (dev, SMEM_DDR_IDELAY_MGT[mem], (uint32_t)data, 0);
  restore_vtc (dev, mem, vtc_read);
}

/*
 * Read delay value for line
 */
uint32_t
read_delay (xildev *dev, unsigned int mem, int line)
{
  uint32_t delay = 0;
  int vtc_read = disable_vtc (dev, mem);
  uint32_t dq_path = (line << 12);
  // DDR3 DQ Selection
  xil_write_reg (dev, SMEM_DDR_IDELAY_VAL[mem], (uint32_t)dq_path, 0);
  // Read delay value from DELQ register
  xil_read_reg (dev, SMEM_DDR_IDELAY_VAL[mem], (uint32_t *)&delay, 0);
  delay = delay & DELAY_MASK;
  restore_vtc (dev, mem, vtc_read);
  return delay;
}

ssize_t
read_to_buffer (const char *fname, int fd, char *buffer, uint64_t size,
                uint64_t base)
{
  ssize_t rc;
  uint64_t count = 0;
  char *buf = buffer;
  off_t offset = base;
  int loop = 0;

  while (count < size)
    {
      uint64_t bytes = size - count;

      if (bytes > RW_MAX_SIZE)
        bytes = RW_MAX_SIZE;

      if (offset)
        {
          rc = lseek (fd, offset, SEEK_SET);
          if (rc != offset)
            {
              fprintf (stderr, "%s, seek off 0x%lx != 0x%lx.\n", fname, rc,
                       offset);
              perror ("seek file");
              return -EIO;
            }
        }

      /* read data from file into memory buffer */
      rc = read (fd, buf, bytes);
      if (rc < 0)
        {
          fprintf (stderr, "%s, read 0x%lx @ 0x%lx failed %ld.\n", fname,
                   bytes, offset, rc);
          perror ("read file");
          return -EIO;
        }

      count += rc;
      if (rc != bytes)
        {
          fprintf (stderr, "%s, read underflow 0x%lx/0x%lx @ 0x%lx.\n", fname,
                   rc, bytes, offset);
          break;
        }

      buf += bytes;
      offset += bytes;
      loop++;
    }

  if (count != size && loop)
    fprintf (stderr, "%s, read underflow 0x%lx/0x%lx.\n", fname, count, size);
  return count;
}

ssize_t
write_from_buffer (const char *fname, int fd, char *buffer, uint64_t size,
                   uint64_t base)
{
  ssize_t rc;
  uint64_t count = 0;
  char *buf = buffer;
  off_t offset = base;
  int loop = 0;

  while (count < size)
    {
      uint64_t bytes = size - count;

      if (bytes > RW_MAX_SIZE)
        bytes = RW_MAX_SIZE;

      if (offset)
        {
          rc = lseek (fd, offset, SEEK_SET);
          if (rc != offset)
            {
              fprintf (stderr, "%s, seek off 0x%lx != 0x%lx.\n", fname, rc,
                       offset);
              perror ("seek file");
              return -EIO;
            }
        }

      /* write data to file from memory buffer */
      rc = write (fd, buf, bytes);
      if (rc < 0)
        {
          fprintf (stderr, "%s, write 0x%lx @ 0x%lx failed %ld.\n", fname,
                   bytes, offset, rc);
          perror ("write file");
          return -EIO;
        }

      count += rc;
      if (rc != bytes)
        {
          fprintf (stderr, "%s, write underflow 0x%lx/0x%lx @ 0x%lx.\n", fname,
                   rc, bytes, offset);
          break;
        }
      buf += bytes;
      offset += bytes;

      loop++;
    }

  if (count != size && loop)
    fprintf (stderr, "%s, write underflow 0x%lx/0x%lx.\n", fname, count, size);

  return count;
}

int
read_dma (const char *devname, uint64_t addr, uint64_t size, uint64_t offset,
          unsigned int *dest_buf)
{
  ssize_t rc = 0;
  size_t bytes_done = 0;
  uint64_t i = 0;
  char *buffer = NULL;
  char *allocated = NULL;
  int fpga_fd;
  int underflow = 0;

  fpga_fd = open (devname, O_RDWR);
  if (fpga_fd < 0)
    {
      fprintf (stderr, "unable to open device %s, %d.\n", devname, fpga_fd);
      perror ("open device");
      return -EINVAL;
    }

  posix_memalign ((void **)&allocated, 4096 /*alignment */, size + 4096);
  if (!allocated)
    {
      fprintf (stderr, "OOM %lu.\n", size + 4096);
      rc = -ENOMEM;
      goto out;
    }

  buffer = allocated + offset;

  rc = read_to_buffer (devname, fpga_fd, buffer, size, addr);
  if (rc < 0)
    goto out;
  bytes_done = rc;

  if (bytes_done < size)
    {
      fprintf (stderr, "#%ld: underflow %ld/%ld.\n", i, bytes_done, size);
      underflow = 1;
    }

  memcpy (dest_buf, buffer, size);

  if (!underflow)
    {
      rc = 0;
    }
  else if (eop_flush)
    {
      /* allow underflow with -e option */
      rc = 0;
    }
  else
    rc = -EIO;

out:
  free (allocated);
  close (fpga_fd);
  return rc;
}

int
write_dma (const char *devname, uint64_t addr, uint64_t size, uint64_t offset,
           unsigned int *src_buf)
{
  uint64_t i = 0;
  ssize_t rc;
  size_t bytes_done = 0;
  char *buffer = NULL;
  char *allocated = NULL;

  int underflow = 0;

  int fpga_fd = open (devname, O_RDWR);
  if (fpga_fd < 0)
    {
      fprintf (stderr, "unable to open device %s, %d.\n", devname, fpga_fd);
      perror ("open device");
      return -EINVAL;
    }

  posix_memalign ((void **)&allocated, 4096 /*alignment */, size + 4096);
  if (!allocated)
    {
      fprintf (stderr, "OOM %lu.\n", size + 4096);
      rc = -ENOMEM;
      goto out;
    }

  buffer = allocated + offset;

  // copy data to write buff
  memcpy (buffer, src_buf, (size_t)size);

  rc = write_from_buffer (devname, fpga_fd, buffer, size, addr);
  if (rc < 0)
    goto out;
  bytes_done = rc;

  if (bytes_done < size)
    {
      printf ("#%ld: underflow %ld/%ld.\n", i, bytes_done, size);
      underflow = 1;
    }

out:
  close (fpga_fd);
  free (allocated);

  if (rc < 0)
    return rc;
  /* treat underflow as error */
  return underflow ? -EIO : 0;
}
