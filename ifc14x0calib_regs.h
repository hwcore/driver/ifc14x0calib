#ifndef _IFC14X0_CALIB_REGS_H_
#define _IFC14X0_CALIB_REGS_H_

/*
 *  DDR3 CSR (CSR + $800/C)
 */
#define TSC_SMEM_DDR3_SIZE(x)                                                 \
  ((x & 0xc) ? (0x8000000 << ((x >> 2) & 3))                                  \
             : 0) /* calculate SMEM DDR3 size            */

/*
 *  DDR3 CSR (CSR + $814)
 */
#define TSC_SMEM_DDR3_CALSEM_ABSENT (1 << 29)
#define TSC_SMEM_DDR3_CALSEM_OWNED (1 << 30)
#define TSC_SMEM_DDR3_CALSEM_DONE (1 << 31)

/*
 *  SRAM CSR (CSR + $810/C)
 */
// #define TSC_SMEM_RAM_SIZE(x) ((x&0xc)?(0x10000<<((x>>2)&3)):0)  /* calculate
// SMEM RAM size            */
#define TSC_SMEM_RAM_SIZE(x)                                                  \
  (0x10000 << ((x >> 2) & 3)) /* calculate SMEM RAM size            */

// DDR3 Bank 1
#define AXI_SMEM_BASE 0x410000
#define AXI_SMEM_DDR3_CSR 0x410010
#define AXI_SMEM_DDR3_ERR 0x410014 // Not yet implemented in FW.
#define AXI_SMEM_DDR3_IDELAY_MGT_1 0x410018
#define AXI_SMEM_DDR3_IDELAY_VAL_1 0x41001C
#define AXI_SMEM_DDR3_IDELAY_MGT_2 0x410020
#define AXI_SMEM_DDR3_IDELAY_VAL_2 0x410024

// DDR3 Read to FPGA register sampling point
#define INIT_DDR_BIT_SLIP0 0x00002020
#define INIT_DDR_BIT_SLIP1 0x00002060
#define INIT_DDR_BIT_SLIP2 0x000020A0
#define INIT_DDR_BIT_SLIP3 0x000020E0

#endif //_IFC14X0_CALIB_REGS_H_
