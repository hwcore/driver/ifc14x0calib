/*
 *  References
 *
 *    filename : ifc14x0calib.c
 *    author   : Joao Paulo Martins
 *    company  : ESS ERIC
 *    creation : Sep 2022
 *
 *----------------------------------------------------------------------------
 *  Description
 *
 *    This file contains the core of the DDR Calibration function for IFC14x0
 *    Based on SmemCalibration.c from IOxOS
 *
 *----------------------------------------------------------------------------
 *
 *  Copyright (C) IOxOS Technologies SA <ioxos@ioxos.ch>
 *  Copyright (C) European Spallation Source ERIC
 *
 *    THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 *    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 *PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE
 *LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *POSSIBILITY OF SUCH DAMAGE.
 *
 *    GPL license :
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU General Public License
 *    as published by the Free Software Foundation; either version 2
 *    of the License, or (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307,
 *USA.
 *
 */

#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <limits.h>
#include <pty.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include <dma_utils.h>
#include <libxildrv.h>

#include "ifc14x0calib_regs.h"

#define MINDEX (mem + m - 1)

#define PATTERN_SIZE 32
#define BUFFER_SIZE 64
#define CURRENT_STEP 4
#define DEFAULT_DELAY                                                         \
  0x040 // Should not be lower than 0x040, according to Xilinx
#define MAX_DELAY 0x1ff

/* SMEM control & status register */
#define SMEM_DDR3_CSR AXI_SMEM_DDR3_CSR

int verbose = 0;
#define print_log(...)                                                        \
  if (verbose >= 1)                                                           \
    {                                                                         \
      printf (__VA_ARGS__);                                                   \
    }

#define print_debug(...)                                                      \
  if (verbose >= 2)                                                           \
    {                                                                         \
      printf (__VA_ARGS__);                                                   \
    }

/* get_opt options */
static struct option const long_opts[]
    = { { "device", required_argument, NULL, 'd' },
        { "devicec2h", required_argument, NULL, 'c' },
        { "deviceh2c", required_argument, NULL, 'o' },
        { "help", no_argument, NULL, 'h' },
        { "verbose", required_argument, NULL, 'v' },
        { "memory", required_argument, NULL, 'm' },
        { 0, 0, 0, 0 } };

void
rw_test (const char *dmadeviceh2c, const char *dmadevicec2h, uint64_t address)
{
  uint32_t buffer_t[BUFFER_SIZE] = { 0 };
  uint32_t buffer_r[BUFFER_SIZE] = { 0 };
  int r;
  printf ("Read and write 0xdeadbeef 0xffff0000\n");
  for (r = 0; r < BUFFER_SIZE / 8; r += 2)
    {
      buffer_t[r] = 0xdeadbeef;
      buffer_t[r + 1] = 0xffff0000;
    }
  int dmaresult = write_dma (dmadeviceh2c, address, BUFFER_SIZE, 0, buffer_t);
  if (dmaresult)
    {
      printf ("[ERROR] dma_h2c result = %d\n", dmaresult);
    }

  dmaresult = read_dma (dmadevicec2h, address, BUFFER_SIZE, 0, buffer_r);
  if (dmaresult)
    {
      printf ("[ERROR] dma_c2h result = %d\n", dmaresult);
    }
  else
    {
      for (r = 0; r < 16; r++)
        {
          printf ("0x%08x ", buffer_r[r]);
        }
    }
  printf ("\n");
}

void
rw_word (const char *dmadeviceh2c, const char *dmadevicec2h, uint64_t address)
{
  uint64_t buffer_t = LONG_MAX;
  uint64_t buffer_r = 0;

  printf ("64bit write test\nWrite: %ld\n", buffer_t);
  int dmaresult = write_dma (dmadeviceh2c, address, 8, 0, (uint *)&buffer_t);
  if (dmaresult)
    {
      printf ("[ERROR] dma_h2c result = %d\n", dmaresult);
    }

  dmaresult = read_dma (dmadevicec2h, address, 8, 0, (uint *)&buffer_r);
  if (dmaresult)
    {
      printf ("[ERROR] dma_c2h result = %d\n", dmaresult);
    }
  else
    {
      printf ("Read: %ld\n", buffer_r);
    }
}

void
rw_string (const char *dmadeviceh2c, const char *dmadevicec2h,
           uint64_t address)
{
  char buffer_t[28] = "Hello World! Am I working?";
  int size = sizeof (buffer_t);
  char *buffer_r = (char *)malloc (sizeof (buffer_t));
  int dmaresult
      = write_dma (dmadeviceh2c, address, size, 0, (unsigned int *)buffer_t);
  if (dmaresult)
    {
      printf ("[ERROR] dma_h2c result = %d\n", dmaresult);
    }

  dmaresult
      = read_dma (dmadevicec2h, address, size, 0, (unsigned int *)buffer_r);
  if (dmaresult)
    {
      printf ("[ERROR] dma_c2h result = %d\n", dmaresult);
    }
  else
    {
      printf ("String test\n");
      printf ("%28s\n", buffer_r);
    }
  free (buffer_r);
}

void
print_dma_buffer (unsigned int *buffer, unsigned int size)
{
  if (verbose < 2)
    return;

  int i = 0;
  for (i = 0; i < size; i++)
    {
      printf ("0x%08x ", buffer[i]);
    }
}

int
pattern_check (unsigned int *ref_pattern, unsigned int line,
               unsigned int *buf_rx, unsigned int *buf_tx)
{
  unsigned int pattern[PATTERN_SIZE] = { 0 };
  /*
   * This is calibrating each DQ, that transfers 32 bits each. It
   * is getting the bit correspondent to the current DQ and
   * putting on a pattern that should be the same for every DQ. It
   * compares with the reference pattern and steps the delay and
   * loop. The calibration is done when it finishes the loop and
   * gets the middle delay for the working range.
   */
  for (int l = 0; l < DQ_LINES; l++)
    {
      pattern[l * 2] = (buf_rx[l] & (0x1 << line)) >> line;
      pattern[(l * 2) + 1]
          = (buf_rx[l] & (0x1 << (DQ_LINES + line))) >> (DQ_LINES + line);
      print_log ("\n line[%02d]  | l[%02d] | buf_tx[0x%08x] buffer_rx "
                 "[0x%08x]  | pattern [0x%08x] \n",
                 line, l, buf_tx[l], buf_rx[l], pattern[l]);
    }

  return memcmp (pattern, ref_pattern, PATTERN_SIZE * sizeof (int));
}

void
read_and_write_dma (const char *dmadevicec2h, const char *dmadeviceh2c,
                    uint64_t address, uint64_t size, unsigned int *buf_tx,
                    unsigned int *buf_rx, unsigned int line)

{
  int dmaresult = -1;
  /* Fill DDR3 with test pattern */
  dmaresult = write_dma (dmadeviceh2c, address, size, 0, buf_tx);
  if (dmaresult)
    {
      printf ("[ERROR] dma_h2c result = %d\n", dmaresult);
    }
  else
    {
      print_debug ("----------- DMA HOST 2 CARD  "
                   "-------------------------------------------------\n");
      print_dma_buffer (buf_tx, DQ_LINES);
      print_debug ("\n--------------------------------------------------------"
                   "-------------------\n");
    }
  /* Get data from DDR3 */
  dmaresult = read_dma (dmadevicec2h, address, size, 0, buf_rx);
  if (dmaresult)
    {
      printf ("[ERROR] dma_c2h result = %d\n", dmaresult);
    }
  else
    {
      print_debug (
          "DQ[%02d] - DMA CARD 2 HOST ************************************\n",
          line);
      print_dma_buffer (buf_rx, DQ_LINES);
      print_debug ("\n****************************************************\n");
    }
}

static void
usage (const char *name)
{
  int i = 0;

  fprintf (stdout, "%s\n\n", name);
  fprintf (stdout, "usage: %s [OPTIONS]\n\n", name);
  fprintf (stdout, "IFC14x0 - DDR3 write leveling (calibration)\n\n");

  /* device */
  fprintf (stdout, "  -%c (--%s) device (defaults to %s)\n", long_opts[i].val,
           long_opts[i].name, DEVICE_NAME_DEFAULT);
  i++;

  /* device */
  fprintf (stdout, "  -%c (--%s) devicec2h (defaults to %s)\n",
           long_opts[i].val, long_opts[i].name, DEVICE_C2H_NAME_DEFAULT);
  i++;

  /* device */
  fprintf (stdout, "  -%c (--%s) deviceh2c (defaults to %s)\n",
           long_opts[i].val, long_opts[i].name, DEVICE_H2C_NAME_DEFAULT);
  i++;

  /* help */
  fprintf (stdout, "  -%c (--%s) print usage help and exit\n",
           long_opts[i].val, long_opts[i].name);
  i++;

  /* verbose */
  fprintf (stdout,
           "  -%c (--%s) verbose output (1 = reg. dump, 2 = mem dump)\n",
           long_opts[i].val, long_opts[i].name);
  i++;

  /* memory */
  fprintf (stdout,
           "  -%c (--%s) memory bank (1 = SMEM1, 2 = SMEM2, 3 = both and "
           "default)\n",
           long_opts[i].val, long_opts[i].name);
  i++;
}

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * Function name : main
 * Prototype     : int
 * Parameters    : -
 * Return        : -
 *----------------------------------------------------------------------------
 * Description   : Main executable
 *
 *++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
int
main (int argc, char *argv[])
{

  int cmd_opt;
  char *device = DEVICE_NAME_DEFAULT;
  const char *dmadevicec2h = DEVICE_C2H_NAME_DEFAULT;
  const char *dmadeviceh2c = DEVICE_H2C_NAME_DEFAULT;
  uint64_t address = 0;
  verbose = 0;
  int memory = 3;

  xildev *dev = NULL;

  while (
      (cmd_opt = getopt_long (argc, argv, "hv:m:d:a:k:s:o:", long_opts, NULL))
      != -1)
    {
      switch (cmd_opt)
        {
        case 0:
          /* long option */
          break;
        case 'd':
          /* device node name */
          device = strdup (optarg);
          break;
        case 'c':
          /* device node name */
          dmadevicec2h = strdup (optarg);
          break;
        case 'o':
          /* device node name */
          dmadeviceh2c = strdup (optarg);
          break;
        case 'm':
          memory = getopt_integer (optarg);
          if (memory < 2)
            memory = 1;
          if (memory > 3)
            memory = 3;
          break;

        /* print usage help and exit */
        case 'v':
          verbose = getopt_integer (optarg);
          if (verbose > 2)
            verbose = 2;
          if (verbose < 0)
            verbose = 0;
          break;
        case 'h':
        default:
          usage (argv[0]);
          exit (0);
          break;
        }
    }

  print_log ("[IFC14x0calib] dev %s, memory %d\n", device, memory);

  dev = xil_open_device (device);

  if (!dev)
    {
      fprintf (stderr, "[IFC14x0calib] Can't open device '%s' due to '%s'\n",
               argv[optind], strerror (errno));
      return -1;
    }

  if (access (dmadevicec2h, F_OK) == -1)
    {
      perror ("Can't access card to host device");
      return -1;
    }

  if (access (dmadeviceh2c, F_OK) == -1)
    {
      perror ("Can't access host to card device");
      return -1;
    }

  /* IDEL adjustment register for both DDR3 memory */
  const unsigned int SMEM_DDR_IDELAY_VAL[2]
      = { AXI_SMEM_DDR3_IDELAY_VAL_1, AXI_SMEM_DDR3_IDELAY_VAL_2 };

  /* IDEL control register for both DDR3 memory */
  const unsigned int SMEM_DDR_IDELAY_MGT[2]
      = { AXI_SMEM_DDR3_IDELAY_MGT_1, AXI_SMEM_DDR3_IDELAY_MGT_2 };

  const unsigned int DMA_ADDRESS[2] = { DMA_ADDRESS_SMEM1, DMA_ADDRESS_SMEM2 };

  int DQ_NOK[DQ_LINES] = { 0 };
  unsigned int buf_tx[BUFFER_SIZE]
      = { 0 }; // Locally buffer to send data to DDR3
  unsigned int buf_rx[BUFFER_SIZE]
      = { 0 }; // Locally buffer to receive data from DDR3
  uint32_t data = 0;
  int cnt_value = 0;
  unsigned int init_delay[2][DQ_LINES] = { 0 };
  unsigned int final_cnt_value_store[DQ_LINES] = { 0 };
  unsigned int memory_banks = 0;
  unsigned int m = 0; // Loop increment
  unsigned int delay_step = 0;
  unsigned int board_delay = 0;
  unsigned int start = 0;                // Save the start index
  unsigned int last_delay = 0;           // Save the last_delay index
  unsigned int working_delays_count = 0; // Count the number of passed test "1"
  unsigned int avg_x = 0;  // Horizontal DQ average delay for 16 lines of DQ
  unsigned int marker = 0; // Final delay marker value per line
  unsigned int NOK = 1;    // Calibration is done or not
  unsigned int ref_pattern[PATTERN_SIZE]
      = { 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0,
          1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1 };

  /*
   * 32 bits reference pattern for each DQ :
   *    0101 1001 0011 0100 1011 0101 1001 0011
   */
  unsigned int ref_word[DQ_LINES];
  ref_word[0] = 0xffff0000;  // 1111 1111 1111 1111 0000 0000 0000 0000
  ref_word[1] = 0xffff0000;  // 1111 1111 1111 1111 0000 0000 0000 0000
  ref_word[2] = 0x0000ffff;  // 0000 0000 0000 0000 1111 1111 1111 1111
  ref_word[3] = 0xffff0000;  // 1111 1111 1111 1111 0000 0000 0000 0000
  ref_word[4] = 0x00000000;  // 0000 0000 0000 0000 0000 0000 0000 0000
  ref_word[5] = 0xffffffff;  // 1111 1111 1111 1111 1111 1111 1111 1111
  ref_word[6] = 0xffff0000;  // 1111 1111 1111 1111 0000 0000 0000 0000
  ref_word[7] = 0x00000000;  // 0000 0000 0000 0000 0000 0000 0000 0000
  ref_word[8] = 0x0000ffff;  // 0000 0000 0000 0000 1111 1111 1111 1111
  ref_word[9] = 0xffffffff;  // 1111 1111 1111 1111 1111 1111 1111 1111
  ref_word[10] = 0xffff0000; // 1111 1111 1111 1111 0000 0000 0000 0000
  ref_word[11] = 0xffff0000; // 1111 1111 1111 1111 0000 0000 0000 0000
  ref_word[12] = 0x0000ffff; // 0000 0000 0000 0000 1111 1111 1111 1111
  ref_word[13] = 0xffff0000; // 1111 1111 1111 1111 0000 0000 0000 0000
  ref_word[14] = 0x00000000; // 0000 0000 0000 0000 0000 0000 0000 0000
  ref_word[15] = 0xffffffff; // 1111 1111 1111 1111 1111 1111 1111 1111

  /*
   * Startup procedure.
   */

  unsigned int mem = (unsigned int)memory;
  /* Check if calibration is needed for mem1, mem2 or mem1 & mem2 */
  if (mem == 1)
    {
      mem = 1;
      memory_banks = 1;
    }
  else if (mem == 2)
    {
      mem = 2;
      memory_banks = 1;
    }
  else if (mem == 3)
    {
      mem = 1;
      memory_banks = 2;
    }

  printf ("Setting up dma device for calibration\n");
  printf ("DDR PLL/IODELAY reset\n");
  xil_write_reg (dev, SMEM_DDR3_CSR, 0x00008080, verbose);
  usleep (300000);

  printf ("Enabling Memory Controller\n");
  xil_write_reg (dev, SMEM_DDR3_CSR, INIT_DDR_BIT_SLIP1, verbose);
  usleep (1000000);

  printf ("Reading Status\n");
  uint32_t read_ddr_csr;
  xil_read_reg (dev, SMEM_DDR3_CSR, &read_ddr_csr, verbose);

  /* Setup Calibration */
  for (m = 0; m < memory_banks; m++)
    {
      print_log ("Reset calibration register\n");
      data = 0;
      xil_write_reg (dev, SMEM_DDR_IDELAY_VAL[MINDEX], data, verbose);
      xil_write_reg (dev, SMEM_DDR_IDELAY_MGT[MINDEX], data, verbose);

      print_log ("Write an initial value for all IDELAY3 cells in MEM%x : \n",
                 mem + m);
      for (int line = 0; line < DQ_LINES; line++)
        {
          set_delay (dev, MINDEX, DEFAULT_DELAY, line);
        }
      data = 0;

      printf ("Reseting delays for MEM%x : \n", mem + m);
      for (int line = 0; line < DQ_LINES; line++)
        {
          cnt_value = read_delay (dev, MINDEX, line);
          init_delay[m][line] = cnt_value & 0x1ff;
          printf ("DQ[%02i] DELQ register 0x%08x -> Initial delay 0x%03x \n",
                  line, cnt_value, init_delay[m][line]);
        }
    }

  printf ("\n\nStarting Calibration Process\n");
  for (m = 0; m < memory_banks; m++)
    {
      printf ("Calibrating memory bank %d", mem + m);
      address = DMA_ADDRESS[MINDEX];

      memcpy (buf_tx, ref_word, sizeof (ref_word));

      printf ("\n");
      printf ("Default INC           : %d \n", CURRENT_STEP);
      printf ("Default CNT           : %02x \n", DEFAULT_DELAY);
      printf ("\n");

      print_log (
          "\n Delay value [MSB] : 00 00 00 00 00 00 00 00 00 00 00 00 00 00 "
          "00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 "
          "00 00 00 00 00 00 00 00 00 00 00 00 00 01 01 01 01 01 01 01 01 "
          "01 01 01 01 01 01 01 01 01 01 01 01 01 01 01 01 01 01 01 01 01 "
          "01 01 01 01 01 01 01 01 01 01 01 01 01 01 01 01 01 01 01 01 01 "
          "01 01 01 01 01 01 01 01 01 01 01 01 01 01 \n");
      print_log (
          " Delay value [LSB] : 40 44 48 4C 50 54 58 5C 60 64 68 6C 70 74 "
          "78 7C 80 84 88 8C 90 94 98 9C A0 A4 A8 AC B0 B4 B8 BC C0 C4 C8 "
          "CC D0 D4 D8 DC E0 E4 E8 EC F0 F4 F8 FC 00 04 08 0C 10 14 18 1C "
          "20 24 28 2C 30 34 38 3F 40 44 48 4C 50 54 58 5C 60 64 68 6C 70 "
          "74 78 7C 80 84 88 8C 90 94 98 9C A0 A4 A8 AC B0 B4 B8 BC C0 C4 "
          "C8 CC D0 D4 D8 DC E0 E4 E8 EC F0 F4 F8 FC \n");
      print_log (
          "+--------------------------------------------------------------"
          "---------------------------------------------------------------"
          "---------------------------------------------------------------"
          "---------------------------------------------------------------"
          "---------------------------------------------------------------"
          "------------------------------------------+ \n");
      data = 0;
      xil_write_reg (dev, SMEM_DDR_IDELAY_MGT[MINDEX], data, verbose);

      for (int line = 0; line < DQ_LINES; line++)
        {

          print_log (
              "\nStore initial value of count of the DELQ register [%02d] \n",
              line);

          cnt_value = read_delay (dev, MINDEX, line);
          print_log (
              "\nLine %d, delay_step = @0x%08x, initial_delay = 0x%08x \n",
              line, delay_step, cnt_value);
          printf ("\n DQ[%02d] test >>>>> :\n", line);

          /*
           * Reset avg_x, start index, number of test passed "ok" and
           * last_delay value for each DQ
           */
          avg_x = 0;
          last_delay = 0;
          working_delays_count = 0;

          /*
           * Add steps by steps for current DQ from initial count value to max
           */
          for (delay_step = DEFAULT_DELAY; delay_step < MAX_DELAY;
               delay_step += CURRENT_STEP)
            {
              set_delay (dev, MINDEX, delay_step, line);
              print_log ("\n Line %d, delay_step = 0x%03x \n", line,
                         delay_step);

              read_and_write_dma (dmadevicec2h, dmadeviceh2c, address,
                                  sizeof (ref_word), buf_tx, buf_rx, line);
              /* Check data received with reference pattern */
              if (!pattern_check (ref_pattern, line, buf_rx, buf_tx))
                {
                  printf ("Y");
                  last_delay = delay_step;
                  NOK = 0;
                  working_delays_count++;
                }
              else
                {
                  printf ("-");
                }

              board_delay = read_delay (dev, MINDEX, line);
              if (board_delay != delay_step)
                {
                  printf ("Board delay could not be written, %d, %d",
                          board_delay, delay_step);
                  return -1;
                }
            }

          /* If calibration failed set the default count value */
          if (working_delays_count == 0)
            {
              marker = DEFAULT_DELAY;
            }
          /* Update the new count value with the median value */
          else
            {
              /* Compute the start window */
              start = (last_delay - (working_delays_count * CURRENT_STEP))
                      + CURRENT_STEP;
              /* Compute the average of the window */
              avg_x = (working_delays_count * CURRENT_STEP) / 2;
              /* Compute the new delay marker to apply */
              marker = start + avg_x;
            }

          /* Update the array with the new count value */
          final_cnt_value_store[line] = marker;

          /* Trace new delay */
          printf ("\n");
          printf (" Init  delay 0x%03x :\n", init_delay[m][line]);
          printf (" Final delay 0x%03x :", marker);
          printf ("\n");

          print_log ("\n Write count value [%02d] 0x%03x \n", line,
                     final_cnt_value_store[line]);
          set_delay (dev, MINDEX, final_cnt_value_store[line], line);

          /* Check status */
          if (working_delays_count == 0)
            {
              NOK = 1;
              DQ_NOK[line] = 1;
            }
          else
            {
              DQ_NOK[line] = 0;
            }

          /* Set IDEL and DELQ to 0 */
          data = 0;
          xil_write_reg (dev, SMEM_DDR_IDELAY_MGT[MINDEX], data, verbose);
          xil_write_reg (dev, SMEM_DDR_IDELAY_VAL[MINDEX], data, verbose);
        }

      /* Execution is finished OK or NOK */
      printf ("\n");
      if (NOK == 1)
        {
          printf ("Calibration is not possible, error on line(s) : \n");
          for (m = 0; m < DQ_LINES; m++)
            {
              if (DQ_NOK[m] == 1)
                {
                  printf ("DQ[%i] \n", m);
                }
            }
        }

      /* Print initial and final */
      printf ("\n");
      for (int line = 0; line < DQ_LINES; line++)
        {
          printf ("DQ[%02d] - Initial delay 0x%03x - Final delay 0x%03x \n",
                  line, init_delay[m][line], final_cnt_value_store[line]);
        }

      printf ("\n");
      printf ("Calibration finished for memory bank %d ! \n", mem + m);
      printf ("\n");

      /* Set IDEL and DELQ to 0 */
      data = 0;
      xil_write_reg (dev, SMEM_DDR_IDELAY_MGT[MINDEX], data, verbose);
      xil_write_reg (dev, SMEM_DDR_IDELAY_VAL[MINDEX], data, verbose);

    } /* end for */

  printf ("*******************************************************************"
          "********\n");

  /* Loop on SMEM */
  for (m = 0; m < memory_banks; m++)
    {
      /* Reset calibration register */
      data = 0;
      xil_write_reg (dev, SMEM_DDR_IDELAY_VAL[MINDEX], data, verbose);
      xil_write_reg (dev, SMEM_DDR_IDELAY_MGT[MINDEX], data, verbose);
      printf ("Readback values for MEM%x : \n", m);

      /* Loop on 16 DQ */
      for (int line = 0; line < DQ_LINES; line++)
        {
          cnt_value = read_delay (dev, MINDEX, line);
          printf ("DQ[%02i] Initial delay 0x%03x - DELQ register 0x%08x -> "
                  "Final delay 0x%03x \n",
                  line, init_delay[m][line], cnt_value, cnt_value & 0x1ff);
        }

      /* Set IDEL and DELQ to 0 */
      data = 0;
      xil_write_reg (dev, SMEM_DDR_IDELAY_MGT[MINDEX], data, verbose);
      xil_write_reg (dev, SMEM_DDR_IDELAY_VAL[MINDEX], data, verbose);
    }
  printf ("*******************************************************************"
          "********\n");

  /*
   * Dummy read/write procedure for testing purpose.
   */
  for (m = 0; m < memory_banks; m++)
    {
      address = DMA_ADDRESS[m];
      printf ("\n\nRunning test for memory bank %d\n", mem + m);
      rw_test (dmadeviceh2c, dmadevicec2h, address);
      rw_word (dmadeviceh2c, dmadevicec2h, address);
      rw_string (dmadeviceh2c, dmadevicec2h, address);
    }
  printf ("\n");
  xil_close_device (dev);
  return 0;
}
