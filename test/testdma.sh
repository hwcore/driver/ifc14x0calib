#!/usr/bin/bash
XDMATOOLS=/home/joaopaulomartins/dma_ip_drivers/XDMA/linux-kernel/tools

rm *.bin

# Multiplying 9 by 8
let SIZE=14*1024*1024 
echo ${SIZE} 

#DATA="\xEF\xBE\xAD\xDE"
DATA="\xAA\xAA\xAA\xAA"

while true ; do printf ${DATA}; done | dd of=./data_in.bin bs=${SIZE} count=1 iflag=fullblock

echo "Preparing to send to FPGA..."
sleep 2

echo "Sending file to fpga"
${XDMATOOLS}/dma_to_device /dev/xdma0_h2c_0 -f ./data_in.bin  -s ${SIZE} -a 0xc0000000 -c 1

sleep 1
echo "Readback file"
${XDMATOOLS}/dma_from_device /dev/xdma0_c2h_0 -f ./data_out.bin -s ${SIZE} -a 0xc0000000 -c 1

md5sum ./data_in.bin
md5sum ./data_out.bin

