#!/usr/bin/bash

XILDRV=/home/joaopaulomartins/ics-xdriver-lib
export LD_LIBRARY_PATH=${XILDRV}
# Parameters and variables

SMEM_DDR3_CSR=0x00080000
SMEM_DDR3_IFSTA=0x00080008
SMEM_DDR3_IDEL=0x0008000C
DISABLE_VTC=0x10000000
ENABLE_VTC=0x00000001
data=0

# Register description 
#--|---------------------------------------------------------------------------------------|
#--| smem_DDR_CSR (0x0000) -- DDR Control & Status                                         |
#--|---------------------------------------------------------------------------------------|
#--| Bit [] | Name               | R/W | Res | Description                                 |
#--|---------------------------------------------------------------------------------------|
#--| 31:29  | Reserved           | R/W |     | Not used                                    |
#--|    28  | RDATO_FL2          | R   | 0   | DDR3 Arbitration Time-out detected (read 2) |
#--|    27  | WRATO_FL2          | R   | 0   | DDR3 Arbitration Time-out detected (write 2)|
#--|    26  | DLY_Ready (2)      | R   | 0   | DDR3 IO Delay controller 2 is ready OK      |
#--|    25  | DLL_Locked(2)      | R   | 0   | DDR3 associated DCM (DLL 2) is locked       |
#--|    24  | MREADY_2           | R   | 0   | DDR3 Controller 2 Ready                     |
#--| 23:21  | Reserved           | R/w | 0   | Not used                                    |
#--|    20  | RDATO_FL1          | R   | 0   | DDR3 Arbitration Time-out detected (read 1) |
#--|    19  | WRATO_FL1          | R   | 0   | DDR3 Arbitration Time-out detected (write 1)|
#--|    18  | DLY_Ready (1)      | R   | 0   | DDR3 IO Delay controller 1 is ready OK      |
#--|    17  | DLL_Locked(1)      | R   | 0   | DDR3 associated DCM (DLL) 1 is locked       |
#--|    16  | MREADY_1           | R   | 0   | DDR3 Controller 1  Ready                    |
#--|    15  | DDR3_CTR_RST       | R/W | 0   | Reserve                d                    |
#--|    14  | ATO_FLCLRi         | W   | 0   | Arbitration Time-out flags clear            |
#--|    13  | DDR3_Ctr_ENA       | R/W | 1   | Controller Global Enable                    |
#--|    12  | RD_Cache_ENA       | R/W | 0   | READ Ahead cache Global Enable              |
#--| 11: 8  | DDR3_MASK[3:0]     | R/W | 0x0 | Individual Port masking                     |
#--|  7: 6  | DDR3_CTR_BITSLIP   | R/W | 0x0 | Global clock select (250/275-300) MHz       |
#--|     5  | SMEM_ARBMODE[1]    | R/W | 1   | SMEM Multi port access policy - High Port A |
#--|     4  | SMEM_ARBMODE[0]    | R/W | 0   | SMEM Multi port access policy               |
#--|     3  | DDR3_SIZE[1:0]     | R   | 0   | Implemented DDR3 Size                       |
#--|     2  | DDR3_SIZE[1:0]     | R   | 0   | Implemented DDR3 Size                       |
#--|     1  | DDR3_REFMODE       | R/W | 0   | DDR3(L) Refresh Mode control                |
#--|     0  | DDR3_REFRATE       | R/W | 0   | DDR3(L) Refresh Rate control                |
#--|---------------------------------------------------------------------------------------|

echo "DDR PLL/IODELAY reset"
${XILDRV}/xil-rw -w /dev/xdma0 $SMEM_DDR3_CSR 0x00008088
sleep 0.3
echo "Enabling Memory Controller"
${XILDRV}/xil-rw -w /dev/xdma0 $SMEM_DDR3_CSR 0x00002010
sleep 1
echo "Reading Status"
${XILDRV}/xil-rw -r /dev/xdma0 $SMEM_DDR3_CSR 

# Reset calibration register

echo "Reset calibration register"
${XILDRV}/xil-rw -w /dev/xdma0 $SMEM_DDR3_IFSTA $data
${XILDRV}/xil-rw -w /dev/xdma0 $SMEM_DDR3_IDEL $data



echo ""
echo "Calibration"

${XILDRV}/xil-rw -w /dev/xdma0 $SMEM_DDR3_IFSTA 0x00340000
${XILDRV}/xil-rw -w /dev/xdma0 $SMEM_DDR3_IDEL 0x8000FFFF
${XILDRV}/xil-rw -w /dev/xdma0 $SMEM_DDR3_IDEL $DISABLE_VTC
${XILDRV}/xil-rw -r /dev/xdma0 $SMEM_DDR3_IFSTA 
${XILDRV}/xil-rw -w /dev/xdma0 $SMEM_DDR3_IDEL 0x4010FFFF 
${XILDRV}/xil-rw -w /dev/xdma0 $SMEM_DDR3_IDEL $ENABLE_VTC




# for ((j = 0; j < 16; j++))
# do 
#     dq_path=$((j << 12))
#     echo "***********"
#     echo "dq_path $j "
#     echo "***********"
#     ${XILDRV}/xil-rw -w /dev/xdma0 $SMEM_DDR3_IFSTA $dq_path
#     echo ">"
#     ${XILDRV}/xil-rw -w /dev/xdma0 $SMEM_DDR3_IDEL $DISABLE_VTC
#     ${XILDRV}/xil-rw -w /dev/xdma0 $SMEM_DDR3_IFSTA 0x0000100
#     ${XILDRV}/xil-rw -w /dev/xdma0 $SMEM_DDR3_IDEL $dq_path
#     ${XILDRV}/xil-rw -w /dev/xdma0 $SMEM_DDR3_IDEL $ENABLE_VTC
#     sleep 0.5
# done



