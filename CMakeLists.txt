cmake_minimum_required(VERSION 3.4)

# Project information
project(ifc14x0calib)

# General settings
set(CMAKE_VERBOSE_MAKEFILE OFF)
set(CMAKE_BUILD_TYPE Debug)

# Main executable
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -O3 -g -ggdb -std=gnu11")
include_directories(${CMAKE_CURRENT_SOURCE_DIR})
set(SOURCE_FILES ifc14x0calib.c dma_utils.c dma_utils.h libxildrv.c libxildrv.h)
add_executable(ifc14x0calib ${SOURCE_FILES})
