#!/bin/bash
#
# Original https://gist.githubusercontent.com/thelossantos/6e4bd1a2ddd7d55e2a5625413e017460/raw/dc11333f4c2d1c082d9a9d6d6fe728ac599745ec/pcie_fix.bash
#
# We need to discover PCIe bridge attached to AMC board and try to negotiate PCIe link speed
# if Gen3 is not detected.
# lspci provides all the information but needs to be parsed in order to obtain AMC <-> bridge
# relation.
#
# Author : Hinko Kocevar
# Date   : 5 March 2020

[[ $(id -u) == 0 ]] || { echo Must be root to run this script.; exit 1; }

BRIDGES=$(lspci | awk '/PEX 8748/ {print $1;}')
AMCS=$(lspci | awk '/Juelich Device 0024/ {print $1;}')
LINKS=$(lspci -tv | awk '/Juelich Device 0024/ {print;}')

for AMC in $AMCS
do
  AMC_NR=$(echo "$AMC" | cut -c1-2)
  LINK=$(echo "$LINKS" | grep -- "-\[$AMC_NR\]-")
  BRIDGE_ID=$(echo "$LINK" | sed -n 's/.*-\([0-9a-f][0-9a-f]\).[0-9a-f]-.*/\1/p')
  BRIDGE=$(echo "$BRIDGES" | grep ":$BRIDGE_ID.")

  echo "AMC $AMC: connected to bridge $BRIDGE"
  STATUS=$(setpci -s "$BRIDGE" 78.l)
  LNKSTAT=$(echo "$STATUS" | cut -c3-4)

  if [[ $LNKSTAT = '43' ]]; then
    echo "AMC $AMC: PCIe Gen 3 detected! No action needed."
  else
    echo "AMC $AMC: PCIe Gen 1 or 2 detected! Re-negotiating link speed.."
    setpci -s "$BRIDGE" 78.l=20
    STATUS=$(setpci -s "$BRIDGE" 78.l)
    LNKSTAT=$(echo "$STATUS" | cut -c3-4)

    if [[ $LNKSTAT = '43' ]]; then
      echo "AMC $AMC: PCIe Gen 3 detected!"
    else
      echo "AMC $AMC: *** Something went wrong! ***"
    fi
  fi
done

