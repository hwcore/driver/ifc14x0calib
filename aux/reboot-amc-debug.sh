#!/bin/bash
#
# AMC hot cycle
#
# Power cycle the AMC in MCH, then rescan the PCI bus in Linux.
# Essentially resets the firmware and brings it to the inital state.

[[ $(id -u) -eq 0 ]] || { echo "This script must be run as root!"; exit 1; }

ITER=$1
DEV=/dev/sis8300-3
PCISLOT=0000:08:00.0
PCISW=0000:04:03.0
AMC=7
MCH=172.30.150.118

[[ -n $ITER ]] || ITER=1

while [[ $ITER -ge 1 ]]; do

	# tell MCH to power cycle the AMC
	{ sleep 2; \
	echo -e "show_fru\r\n"; sleep 3; \
	echo -e "shutdown $AMC\r\n"; sleep 5; \
	echo -e "fru_start $AMC\r\n"; sleep 5; \
	echo -e "show_fru\r\n"; sleep 3; \
	} | telnet $MCH

	# tell Linux to remove AMC & rescan the PCI bus
	echo "removing AMC #$AMC from PCI slot $PCISLOT"
	echo 1 > /sys/bus/pci/devices/$PCISLOT/remove
	sleep 3
	echo "rescaning PCI bus"
	echo 1 > /sys/bus/pci/rescan
	sleep 2

	# go Gen3
	setpci -s $PCISW 78.l=20 || exit 1
	sleep 2

	# is the card back?
	lspci -s $PCISLOT || exit 1
	lspci -s $PCISLOT -vv | grep LnkSta: || exit 1

	# FW & BPM IDs
	/data/bdee/bpm_devel-stage/bin/sis8300drv_reg -v $DEV 0 || exit 1
	/data/bdee/bpm_devel-stage/bin/sis8300drv_reg -v $DEV 400 || exit 1
	/data/bdee/bpm_devel-stage/bin/sis8300drv_reg -v $DEV 440 || exit 1
	/data/bdee/bpm_devel-stage/bin/sis8300drv_reg -v $DEV 450 || exit 1
	/data/bdee/bpm_devel-stage/bin/sis8300drv_reg -v $DEV 451 -w 2 || exit 1
	/data/bdee/bpm_devel-stage/bin/sis8300drv_reg -v $DEV 450 || exit 1
	/data/bdee/bpm_devel-stage/bin/sis8300drv_reg -v $DEV 452 || exit 1

	/data/bdee/bpm_devel-stage/bin/sis8300drv_reg -v $DEV 404 || exit 1
	/data/bdee/bpm_devel-stage/bin/sis8300drv_reg -v $DEV 405 || exit 1
	/data/bdee/bpm_devel-stage/bin/sis8300drv_reg -v $DEV 406 || exit 1
	# BPM FSM
	/data/bdee/bpm_devel-stage/bin/sis8300drv_reg -v $DEV 402 || exit 1
	sleep 1	

#	echo "restarting the IOC.."
#        systemctl restart e3-ioc-mebtbpm-3.service
#	sleep 5
#	ps -ef | grep cmd

	/data/bdee/bpm_devel-stage/bin/sis8300drv_reg -v $DEV 404 || exit 1
	/data/bdee/bpm_devel-stage/bin/sis8300drv_reg -v $DEV 405 || exit 1
	/data/bdee/bpm_devel-stage/bin/sis8300drv_reg -v $DEV 406 || exit 1
	/data/bdee/bpm_devel-stage/bin/sis8300drv_reg -v $DEV 402 || exit 1
	sleep 1
	/data/bdee/bpm_devel-stage/bin/sis8300drv_reg -v $DEV 402 || exit 1

	# data channel
	/data/bdee/bpm_devel-stage/bin/sis8300drv_reg -v $DEV 553 || exit 1
	/data/bdee/bpm_devel-stage/bin/sis8300drv_reg -v $DEV 553 || exit 1
	/data/bdee/bpm_devel-stage/bin/sis8300drv_reg -v $DEV 450 || exit 1
	/data/bdee/bpm_devel-stage/bin/sis8300drv_reg -v $DEV 451 -w 2 || exit 1
	/data/bdee/bpm_devel-stage/bin/sis8300drv_reg -v $DEV 450 || exit 1
	/data/bdee/bpm_devel-stage/bin/sis8300drv_reg -v $DEV 452 || exit 1

	sleep 4
	# get some data
	#/data/bdee/bpm_devel-stage/bin/sis8300drvbpm_acq $DEV -a -b -n 300000 || exit 1
	#/data/bdee/bpm_devel-stage/bin/sis8300drvbpm_acq $DEV
	#/data/bdee/bpm_devel-stage/bin/sis8300drvbpm_acq $DEV
	/data/bdee/bpm_devel-stage/bin/sis8300drvbpm_acq $DEV -l 10 || exit 1

	/data/bdee/bpm_devel-stage/bin/sis8300drv_reg -v $DEV 404 || exit 1
	/data/bdee/bpm_devel-stage/bin/sis8300drv_reg -v $DEV 405 || exit 1
	/data/bdee/bpm_devel-stage/bin/sis8300drv_reg -v $DEV 406 || exit 1
	/data/bdee/bpm_devel-stage/bin/sis8300drv_reg -v $DEV 450 || exit 1
	/data/bdee/bpm_devel-stage/bin/sis8300drv_reg -v $DEV 451 -w 2 || exit 1
	/data/bdee/bpm_devel-stage/bin/sis8300drv_reg -v $DEV 450 || exit 1
	/data/bdee/bpm_devel-stage/bin/sis8300drv_reg -v $DEV 452 || exit 1

	ITER=$((ITER - 1))
	sleep 4
done

echo "DONE"
exit 0


